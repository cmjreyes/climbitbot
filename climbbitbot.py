import time
import calendar
import praw
import sys

POST_TITLE = 'Friday New Climber Thread'
POST_TEXT = 'Ask Questions!'
USER_AGENT = 'Climbit Test Bot 0.1 by /u/sherlok'
SUBREDDIT = 'sherloktest'
ALERT_TITLE = 'New Question!'

# we store the id of the last new climbers post
current_post = None
processed_root_comments = []
users_to_alert = [
        'sherlok'
        ]

# Login
user_agent = (USER_AGENT);
r = praw.Reddit(user_agent=user_agent)
r.login()

# Run forever - but sleep long. Could be cronned I suppose
while True:
    if current_post is None:
        #TODO - needs to be able to find posts after a name change
        # find last new climbers thread if we don't have a valid id
        matches = r.search(POST_TITLE, SUBREDDIT, 'new', 'cloudsearch', 'week')
    
        for match in matches:
            current_post = match;
    
    # if we're still None - then we don't have a post
    if current_post is None:
        print('Creating new post...')

        # this is probably going to hit a captcha - no biggie for now
        current_post = r.submit(SUBREDDIT, POST_TITLE, POST_TEXT, None, None, True, None)

        # sleep a bit to give things time to happen - no rush
        # then we should fall into monitoring.
        print("Sleeping 2 minutes")
        time.sleep(180)
    else:
        # refresh the data we have for this current post
        current_post.refresh()

        # track any new posts so that we can batch send at the end
        new_posts = []

        # Otherwise we can continue with processing the current thread
        print('Scanning ' + str(len(current_post.comments)) + ' Comments for Changes...')
    
        # populate our comment array with any new posts
        for comment in current_post.comments:
            link = comment.permalink
            if link not in processed_root_comments:
                # alert the mods to a new comment
                print("Found New Message: " + comment.permalink)
    
                # store it in our array so as to not annoy mods
                processed_root_comments.append(link)
                new_posts.append(link)
            else:
                # links been processed - so move on
                print("Seen it " + comment.submission.short_link)
    
        if len(new_posts) > 0:
            print("Sending out notifications for " + str(len(new_posts)) + " questions")
            # format the new questions for easy clicking
            message_body = "\n".join(new_posts)

            # send out any new posts
            for user in users_to_alert:
                r.send_message(user, ALERT_TITLE, message_body)
        else:
            print("No Changes Found...")              

        # sleep for 2 minutes
        print("Sleeping 2 minutes")
        time.sleep(180)
        #time.sleep(36000)
        
            
